const express = require('express')
const router = express.Router()
const roleController = require('./controllers/roleController');

router.get('/role/list', roleController.list)
router.post('/role/create', roleController.create)
router.put('/role/update', roleController.update)
router.delete('/role/destroy', roleController.destroy)

module.exports = router